#### Cifra de César

É conhecida como cifra de troca é uma das mais simples e conhecidas técnicas de criptografia.
É um tipo de cifra de substituição, onde cada letra do texto é substituida por outra do alfabeto.
Exemplo utilizando uma chave 33, A seria substituido pelo H e o B seria substituido por I, e assim por diante.

[Chave 33]

[ABCDEFGHIJKLMNOPQRSTUVWXYZ]

[HIJKLMNOPQRSTUVWXYZABCDEFG]

#### Funcionalidade do Sistema:

O sistema é composto por duas funções cipher e de decipher.
Para cifrar o usuário deve digitar um texto no campo solicitado e o resultado é impresso na pagina HTML.

Decifrar está funcionalidade está em desenvolvimento, o usuário deverá clicar em um botão na pagina HTML e o resultado decifrado seráé impresso na pagina HTML.

#### Desenvolvimento:

Para o desenvolvimento foi utilizado a linguagem de programação JavaScript.
O usuário digitará uma string e através da cifra de cesar o programa retornará uma mensagem cifrada ou decifrada.
O programa é composto por 3 arquivos:

index.html: Página principal onde está presente o html no qual imprime a mensagem criptografada;
app.JavaScript: pagina com as funções de cipher e de decipher;
style.css: Em andamento;

#### Funções:

cifraCesar() | Utilizada para identificar se os texto digitado é maiúsculo ou minusculo;

cipher() | Método principal que criptografa a mensagem.
cifraDeCesar

![Fluxograma](fluxograma.jpg)