var mensagem = prompt("Digite uma frase!");
if (mensagem === "") {
  alert("Você precisa digitar apenas letras!");
}

document.getElementById('name'). innerHTML = cipher(mensagem);

function cifraCesar(charCode) {
  var cifraCesar = charCode - 65 + 33 % 26 + 65;
  if (charCode < 91) {
    if (cifraCesar > 90) {
      return cifraCesar - 91 + 65;
    }
    return cifraCesar;
  }
  if (charCode > 95) {
    if (cifraCesar > 122) {
      return cifraCesar - 123 + 97;
    }
  }
  return cifraCesar;
}

function cipher(string) {
  var result = "";
  for (var i = 0; i < string.length; i++) {
    result += String.fromCharCode(cifraCesar(string.charCodeAt(i)));
  }
  return result;
}

console.log(cipher(mensagem));

function decipher(string) {
  var cifraCesar = 0 - 26 % 26;
  var resultado = "";
  for (var i = 0; i < string.length; i++) {
    resultado += String.fromCharCode(string.charCodeAt(i) + cifraCesar);
  }
  return resultado;
}

console.log(decipher(mensagem));

// HIJKLMNOPQRSTUVWXYZABCDEFG
// ABCDEFGHIJKLMNOPQRSTUVWXYZ
